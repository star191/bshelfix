package star191.bshelfix.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.MinecraftForge;
import star191.bshelfix.BookShelfEventHandler;

public class CommonProxy {
    public void preInit(FMLPreInitializationEvent event)
    {
       MinecraftForge.EVENT_BUS.register(new BookShelfEventHandler());
    }

    public void init(FMLInitializationEvent event)
    {

    }

    public void postInit(FMLPostInitializationEvent event) {

    }
}
