package star191.bshelfix;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;


import java.util.ArrayList;

public class BookShelfEventHandler {

    ArrayList<String> biblioShelfs;

    public BookShelfEventHandler(){

        // add BiblioCraft's shelf's names
        biblioShelfs = new ArrayList<String>();
        biblioShelfs.add("tile.BiblioBookcase");
        biblioShelfs.add("tile.BookcaseFilled");
        biblioShelfs.add("tile.BiblioPotionShelf");
        biblioShelfs.add("tile.BiblioShelf");
        biblioShelfs.add("tile.bookshelf");
    }

    @SubscribeEvent
    public void playerInteractEvent(PlayerInteractEvent event) {

        //-- for indian debug
        // System.out.println("PlayerInteractEvent");

        if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) {

            //-- for indian debug
            // System.out.println("RMB");

            Block blockID = event.entity.worldObj.getBlock(event.x, event.y, event.z);
            if (blockID !=null) {

                //-- for indian debug
                // System.out.println("BlockID:");

                String name = blockID.getUnlocalizedName();

                //-- for indian debug
                // System.out.println(name);

                // if block is BiblioCraft's shelf
                if (biblioShelfs.contains(name)) {

                    //-- for indian debug
                    // System.out.println("Horror BookShelf is found!");

                    // Cancel the event
                    // Player can't interact with the shelf
                    event.setCanceled(true);
                }
            }
        }
    }
}
